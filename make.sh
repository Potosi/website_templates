#!/bin/sh

DATE="$(date +"%D")"
count="$(find src -type f | wc -l)"
OUTDIR=${OUTDIR:-"out"}

md="$(
	command -v md4c ||
	command -v smu ||
	command -v cmark ||
	command -v cmark-gfm
)" || echo "No markdown interpreter found"


header() { cat<<EOF
<!DOCTYPE HTML>
<html>
<head>
<title>Potolandia | Potosi's site</title>
<link rel="stylesheet" type="text/css" href="/style.css">
</head>
<h1>Potolandia</h1>
<a href="/index.html">main page</a>,
<a href="/essays/index.html">essays</a>,
<a href="/filelist.html">file list ($((count)))</a>,
<a href="/single_page.html">single page html</a>,
<a href="/stagit/index.html">repositories</a>
<a>Last updated on $DATE</a>
<hr/>
EOF
}

footer() { cat<<EOF
This website is licensed under <a class="notdead" href="https://creativecommons.org/publicdomain/zero/1.0/">CC0</a> (Public Domain). Contact me at potosi (at) disroot (dot) org
</html>
EOF
}

md() {
	if contains "$1" .md ; then
		"$md" "$@" | sed "s/\.md\"/.html\"/g"
	else
		echo "<pre>" ; cat "$@" ; echo "</pre>"
	fi
}

#mark_dead_links() {
#	# TODO: unfinished, do not use!
#	if ! cat "$1" | grep "<a href="; then return 0; fi
#	input="$(cat "$1")"
#	cd "$(dirname "$1")"
#	for (( i=0; i<${#input}; i++ )); do
#		if [ "${foo:$i:9}" = "<a href=\"" ] ; then
#			startpos="$(($i + 9))"
#			length=0
#			while ! true; do
#				if [ "${input:$(($startpos + $length)):1}" = "\"" ] ; then
#					str="$($startpos + $length)"
#					break
#				fi
#				
#			done
#		fi
#		printf "${input:$i:1}"
#	done
#}

contains() {
	(echo "$1" | grep "$2") > /dev/null
}

echo "Installing to $OUTDIR"

if [ -d "$OUTDIR" ] ; then
	rm -r "$OUTDIR"
fi

mkdir "$OUTDIR"

for d in $(ls -d src/*/ | cut -c 4-); do
	mkdir "$OUTDIR"/"$d"
done

cp -r html/* "$OUTDIR"

header > "$OUTDIR"/single_page.html
(header && echo "<ul>") > "$OUTDIR"/filelist.html

for f in $(find src -type f | sort); do
	printf "Processing %s\n" "$f"
	fname="$(echo "$f" | sed "s/\.md//g" | cut -c 5-)"
	(header "$f"; md "$f" ; echo "<hr>" ; footer) > "$OUTDIR"/"$fname".html
	(echo "<li><a href=\"$fname.html\"> $fname </a></li>") >> "$OUTDIR"/filelist.html
	(echo "<span>$fname</span><br />" ; md "$f" ; echo "<hr>") >> "$OUTDIR"/single_page.html
done
footer >> "$OUTDIR"/single_page.html
(echo "</ul><hr>" && footer) >> "$OUTDIR"/filelist.html
