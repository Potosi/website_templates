#!/bin/sh -e

REPOS="/var/git/Potosi"
OUTDIR=${OUTDIR:-"out"}

echo "Installing to $OUTDIR"

if [ -d "$OUTDIR"/stagit ] ; then
	rm -r "$OUTDIR"/stagit
fi

mkdir -p "$OUTDIR"/stagit

cp -r html/stagit "$OUTDIR"

echo "Generating index"
touch "$OUTDIR"/stagit/index.html
stagit-index $(ls -d $REPOS/* | sort) >> "$OUTDIR"/stagit/index.html

for repo in "$REPOS"/*; do
	echo "Generating stagit pages for $repo"
	(
		mkdir -p "$OUTDIR"/stagit/"$(basename "$repo")"
		cd "$OUTDIR"/stagit/"$(basename "$repo")"
		stagit "$repo"
		ln -s ../style.css style.css
	)
done
