# software

TODO: Finish

Here I document my knowdlege on technology in general.

If you haven't yet noticed, technology is fucked in this timeline. We have been
invaded by propietary spyware that rules our lives, and has brainwashed 99% of
the population. Not only that, but most people who know about this grave
problem support other kinds of degradation of technology, as they have become
so normalized not even they can escape from it. The problems with technology
generally ammount to this (no credit is given to the retarded 99%):

- The intellectual property regime
- Constantly reinventing the wheel
- Software complexity
- "lightweight" instead of simple/suckless
- Forcing new technology into adoption
- For-profit "production" of software
- "Solutions" to problems that didn't exist before
- Religious stallmanism (see the [libreboot.md](libreboot) case)
- Caring more about "usability," "friendliness," and "linux market share" than
software freedom.
- Normalisation of "copyleft" and rejection of permissive licensing
- Thinking software can be good (it's all shit)

Software freedom for me ammounts to two different things:
- The basic rights to redistribute and change the software without complications
- The practical ease to make modifications

Software can't be considered "free" if it is hostile to the practical user, for
example, through the means of intellectual torture by providing software that
is [retarded.md](retarded) in its legibility, usability and complexity, which
just renders it impossible to make modifications, run it on lower-end hardware,
or audit it.

In itself the mere existence of such examples isn't harmful; theoretically good
software will always emerge triumphant, because *the market knows better*, etc.
We can compete with such software, because *those are the rules of the game* 
(??). It's not like we're **being forced** to use shit like that anyways,
right?

Fuck you. Here we know that that is obviously untrue.

We are governed by an oligopoly which claims to be taking the lead in human
progress through modern technology, "AI", cloud, innovation, etc. However none
of these (Facebook, Google, Microsoft, Apple...) could be defined as actual
technology companies, they are purely advertising companies. Their aim is to
have you think that by using their services you are serving the human species,
and stimulating innovation and "the future," when in reality they are using you
as a product, extorting the very last bit of profit they can out of you.

Today software just exists for the fucking sake of it. It doesn't matter how it
is programmed, if it is shit or pure art. Software, because of its scalability
and low production cost, is merely a tool of capitalism to rip off people
through artificial scarcity, planned obsolecense, intellectual property...

But even for the people who develop software for a purpose other than
exploitation of the user, like a large part of the FOSS community, people still
fall trap to the coercive ways, which basically turns them into the same thing
as their propietary counterparts, and why we consider them as an enemy to
software freedom. We can see this clearly in many popular FOSS projects, like
[systemd.md](shitstemd), whose's complexity brings back a software monopoly in
the editability and auditability of the source code, basically rendering the
software propietary. Not to mention that the software's design is violent in
itself, having excessive dependencies which impede it being run on low end
hardware, or being discouraging of customization and forcing users to do things
in specific ways. It's not a coincidence either than most of the software with
these flaws is absolutely corporate, showing that the permissive licensing on
their part is probably is just an act for public image.

A computer doesn't exist for you to run a web browser on it. A computer exists
to [compute.md](**compute**). *And thats it.* If it pushes you away from doing
the computing you like (e.g by using DRM or other kinds of coercive techniques)
then it is no longer a computer: it is a tool of capitalism, it is a machine
set up to enslave you. You don't own your computer, just like nowadays you
probably can't even [self-ownership.md](own yourself.)

You can't be free *from* software if you believe it's actually good. All
software is bad in many aspects, we can only choose to use the less bad
alternatives.

However, to try to mitigate the inevitable effects of human retardation
involving every aspect in software and technology, we encourage radical
minimalism to maximize user power to fully establish autarchic control over
each user's own machine.

## Reject linux

[Linux](linux.md) as an operating system is a failure - firstly because it
isn't an operating system and secondly because it has slowly deteriorated into
another shitty corporate project. Notice how mainstream linux has become thanks
to corporations and cringe influencers - bring back BSD.

### Also reject BSD

Reject [operating systems](os.md) as a whole and embrace simple computing. Keep
that in mind when developing software: do not create dependency on an OS! Still
BSD is probably the best alternative to linux out there, that doesn't mean it
is good. When evaluating software, first make the assumption that 99% is shit
and only some of its ideas must be adopted into our software philosophy. No
software is perfect.

## Better software

Some software that could be used for many small projects that doesn't suck as
much.

### Wayland?

See [wayland](wayland.md). TLDR; fuck wayland and it's "innovations" that
absolutely nobody needed (noone cares about wayland).
