# Package manager

A package manager is a tool which tracks installed software on the system to
be subsequently removed and updated easily.

The purpose of the package manager is to automate the manual task of tracking
files on the system - therefore we support the idea of a package manager on
some machines, as it gives easy control of the system to the user and removes
avoidable effort. A package manager can be extremely suckless and can be
trivially written in POSIX shell, therefore it is not necessarily harmful to
modularity/customizability and should not pose the user any obstacles in
administering their system.

Because of the power of the package manager, it is commonly used to bootstrap
modern operating systems, and it is the whole basis of the file-system
structure.

See [kiss-linux.md](kiss.)
