# POSIX shell

*Not to be confused with the fucking oil company*

You need to learn shell.

But you know what's better than shell?

An operating system whose components can be easily managed through simple
scripts, because [everything is a file](everything_is_a_file.md).

See [plan9](plan9.md)
