# Welcome to potolandia

This is absolutely **not** a blog.

See [howto](howto.md).

This is Potosi's personal website. Here I host my own articles and essays for
no one to see. If you want to contact me personally, email me at potosi (at)
disroot (dot) org, This is an attempt to document my knowdlege in general
without having myself look incredibly stupid at the same time. Often I just
look back at this repository file by file and end up deleting/rewriting
everything after cringing at what I have uploaded (some very stupid content has
made its way here).

I play trumpet and bass, and I'd also be happy to create some public domain
music if you like, please email me. I'd play almost anything but what I usually
litsen is probably progressive post-hardcore.

Here's a list of things I like:

* music
* improvisation (mostly in music)
* [suckless](https://suckless.org)
* [public domain](https://infogalactic.com/info/Public_domain)
* [Counter Economics](https://infogalactic.com/info/Counter_economics)
* [Wikipedia](https://wikipedia.org)
* [OpenBSD](https://openbsd.org)
* [oasislinux](https://github.com/oasislinux/oasis)
* [cat-v](http://cat-v.org)
* [lrs wiki](http://www.tastyfish.cz/lrs/main.html)
* [c4ss](https://c4ss.org)
* [Anarch](https://codeberg.org/drummyfish/Anarch)
* [Esperanto](https://infogalactic.com/info/Esperanto)
* [Libreboot](https://libreboot.org)
* Sometimes I like plan9, sometimes I hate it but not so much.
* Old Thinkpads
* Prog rock
* Jazz fusion

Here's a list of things I **dislike:**

* Religion
* Most cultures
* Property as a right
* Rules
* Politics
* Profit (we call it surplus)
* Business
* Linux
* Frameworks

I am not against [encryption](encryption.md), but against its exaggeratedly
unnecessary and counter-productive implementation in real life.

I also wanted to host some [media](media.md), however I am struggling to find
relevant content to post there - please contribute. TODO.

Think I suck at CSS or web design? See [why](why.md) I made it this way.

This has been greatly inspired by [cat-v.org](http://cat-v.org) and the
[lrs wiki](https://www.tastyfish.cz/lrs/main.html). 

All content is released under CC0 (see [the waiver file](WAIVER.md)). You can
obviously use this as a template of your own website. It is essentially the
make.sh build script and the css stylesheet. It converts markdown files to html
using cmark and creates an optional file list and single page html.

My apporach towards licensing is not contradictory to my [egoism](egoism.md).

The build script is reasonably simple. The site contents in themselves are
written in markdown, which are converted to html using smu, and then uses html
templates defined in simple cat(1) functions for the header and footer.

Some time ago I tried implementing a CGI, however I just ended up using werc.
You can check out its source code looking back in the git history.

Don't hate me for incomplete content, sometimes I write I will do something but
then I just forget (I barely remember what I write here XD).

# status

There are lots of things going on in my mind about this project and many things
are subject to change in the future.

This website is very likely to remain in the darknet only, primarily for the
following reason:

- My current home router+modem is absolutely shit, probably the worst router
to ever be made in human history, I want to fucking kill whoever made this shit,
(in minecraft), it takes literal HOURS to open the router configuration page
(if it even opens at all), the internet also goes off every hour or so, also
ports for the webserver can't be opened properly (port 80 and 8000 are blocked,
while the ones opened for i2p work perfectly). Conclusion, I should probably
set up my own router as on the
[openbsdrouterguide](https://openbsdrouterguide.net) and buy a separate modem,
process which I would gladly document here.

CONCLUSION: Router/modem was fixed for free, router project probably for next
time. I fucking hate the internet.

I also plan to host a git server for my repositories and probably a prebuilt
binary repository for kernels, oasislinux and other packages (perhaps kisslinux
packages?). All of these services would be accessible as a hidden service as
well.

## Why hidden services/darknets?

Darknets are surprisingly simple to set up and consume very little resources.
Obviously it is also very desirable to contribute to the growth of these
networks, adding to the fact that contributors can operate anonymously.

## What services will/might (?) be provided here?

- Anti-Blog
- Monero node (was running previously - changed to OpenBSD and migration
is in progress)
- Git server with "mob" user access with stagit
- Public dumbchat server with full history
- Gopher version of services
- Small suckless online market?
- Suckless gopher HTTP proxy?

Various other useful services might be implemented. Have into account the
resource constraints (Core 2 Quad, 3GB RAM, shitty network connectivity...).

On monero node: sadly monero has a bug on OpenBSD where it segfaults after a
large portion of the lmdb database has been synced. This is incredibly
depressing, as that means I wouldn't be able to use OpenBSD to host the node,
which is desirable due to its focus on security and the nature of monero and
darknets. Adding to that, OpenBSD is exaggeratedly slow on this older pc. I
thought that using linux might be the only option, so I migrated to Alpine. At
the time of writing the monero node is still syncing. It is in pruned node.

I **do not** pretend to recommend these services, this is not the kind of
university project that will fit well in my curriculum. These are simply
services that I have set up for myself that I also happen to forward to the
internet, perhaps to regain just a little bit of the decentralisation the
internet used to have and to of course attract people and share my knowdlege
people might find useful. I also like writing xD. Connecting with people would
also be fun.

Most of the stuff I do here is basically as a learning-through-practice project
if it makes sense.

### Fuck http

Just use gopher.

## What's next on here

Basically a TODO.

* Fuck doing a CGI, look at fucking [werc](http://werc.cat-v.org),
it is fucking usable for fucking once and is in **PUBLIC DOMAIN**
* The media hosting is incredibly desirable as well.
* Gopher proxy page is cool. Implementing a gopher browser is probably very easy.
* Fuck relative links on this website I don't want them anymore.
* Setup an RSS feed / page with diff on document changes?

# Current running services

* personal website
    * TOR: <http://36tdqgy74en5mgwchufq7wfssn4frwuiezgsre5bkrtfioanvtnfm7yd.onion:80>
    * I2P: <http://5bpiqtpjjb5ar34ftqzjuqhqvmrqhhi7hqdhpt6lgiaqmxyi44fa.b32.i2p:80>

* monero node:
    * TOR: <tcp://tryl5hvsamvxqwlhle4bnxm75dz2d5wfcwurwb5rogemhj6qoa5mmnad.onion:18081>
    * I2P: <tcp://4u5cbgatmss5qbsx3xt53dsk7deanksgr2ntcga57w3nu32jt4ma.b32.i2p:18081>

* git (read only):
    * TOR: <git://bqqa6ol6r7rzq5flcugzk5zrpe3xpx57lez3eojv3tuiawebpuy6yiad.onion:9418>
    * I2P: <git://new2jiz3izlhlbwqptjbqrd26khvf4wr5gdmu7pb3pqk52qxvzwa.b32.i2p:9418>

I cannot guarantee that all of these services would be up at all times.
