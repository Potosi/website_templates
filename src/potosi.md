# Potosi

{ me :) ~potosi }

My name is Miguel Blasco, free market anti-capitalist anarchist from Madrid,
Spain. This is a personal page which records my general worldview and opinions,
which you are free to edit.

I play trumpet and bass, and I'd also be happy to create some public domain
music if you like, please email me. My main interests lie on jazz fusion and
progressive rock, although I enjoy litsening to any other kinds of music too.

Here's a list of things I like:

* music
* improvisation (mostly in music)
* [suckless](https://suckless.org)
* [public domain](https://infogalactic.com/info/Public_domain)
* [Counter Economics](https://infogalactic.com/info/Counter_economics)
* [Wikipedia](https://wikipedia.org)
* [OpenBSD](https://openbsd.org)
* [oasislinux](https://github.com/oasislinux/oasis)
* [cat-v](http://cat-v.org)
* [lrs wiki](http://www.tastyfish.cz/lrs/main.html)
* [c4ss](https://c4ss.org)
* [Anarch](https://codeberg.org/drummyfish/Anarch)
* [Esperanto](https://infogalactic.com/info/Esperanto)
* [Libreboot](https://libreboot.org)
* Old Thinkpads
* Prog rock
* Jazz fusion

Here's a list of things I **dislike:**

* Religion
* National cultures
* Intellectual "property"
* Rulers
* Electoral "politics"
* Profit
* Business
* Capitalism
* Linux
* Frameworks

Here's my GPG public key:

    -----BEGIN PGP PUBLIC KEY BLOCK-----
    mDMEZpKA5BYJKwYBBAHaRw8BAQdAzPkZNhHXQo6K2XxI7Th4+nm3eAIJO9iWrv3K
    KNnVWcq0PU1pZ3VlbCBCbGFzY28gKE1pZ3VlbCBCbGFzY28gYWthIFBvdG9zaSkg
    PHBvdG9zaUBkaXNyb290Lm9yZz6ImQQTFgoAQRYhBIPsg2Mmvap25mCQTYpQrhXr
    GdTXBQJmkoDkAhsDBQkA7U4ABQsJCAcCAiICBhUKCQgLAgQWAgMBAh4HAheAAAoJ
    EIpQrhXrGdTXR3YBANbAVuvTU3Ky7rzTTrgV2RJVyl6vI0RyflZXrHk2qE0cAPoD
    Oc5WI6fI0eYM0AjZ/FyiwTpyyoUvG6p1HNf4TidHDrg4BGaSgOQSCisGAQQBl1UB
    BQEBB0DHM3sqLO70SPJNgISU7clBJAQVXqprw52lKF+co4usGQMBCAeIfgQYFgoA
    JhYhBIPsg2Mmvap25mCQTYpQrhXrGdTXBQJmkoDkAhsMBQkA7U4AAAoJEIpQrhXr
    GdTXV1sA/jhRug9nylP+xHT8N53sWzdMaWnoJgwJRplhuS7MjjR9AQC2DHMOjm8s
    Z6dC1YNohqnJpCFm81YJD9F7Xycdso7YDQ==
    =veZJ
    -----END PGP PUBLIC KEY BLOCK-----
