# media

This is an html page which presents some media contents like images or videos
Everything in this page is accesible with with unrestricted read-write access.

There are no restrictions on the content that is allowed on this page (or any
page in this website). Feel free to add, remove, delete, censor, rewrite
everything you can. Add your photos, your music, your audios... Anything.

If there is something that could put me in jail, I might probably remove it,
although I absolutely **do not condone** illegal activities here, and I will
absolutely not prevent you from pushing it here in the first place.

Of course, if you disagree with the way I implemented this, please just push
your changes: you don't even need to ask.

## This is not implemented as-of-yet

Due to this website's somewhat limiting structure, I should probably host this
somewhere else. However, this is 100% on my TODO list.
