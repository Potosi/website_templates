# website

This is a website. Use this however you want.

This looks a lot like the lrs wiki. It used to look more like cat-v's, just
look through the git repo's "old" directory on the previous commits where it
still existed (look back in the git history).

TODO:
- small wiki (lrs-style, done)
- image/video hosting
- forum
- personal site (done)

make.sh will process the md and produce them in this structure:
* personal site: html/
* wiki: html/wiki
* forum: html/forum
* media: html/media

## Licensing

The is released under the Creative Commons Zero waiver. An additional waiver is added:

> The intent of this waiver is to ensure that this work will never be encumbered by any exclusive
> intellectual property rights and will always be in the public domain world-wide, i.e. not putting
> any restrictions on its use.
> 
> Each contributor to this work agrees that they waive any exclusive rights, including but not
> limited to copyright, patents, trademark, trade dress, industrial design, plant varieties and
> trade secrets, to any and all ideas, concepts, processes, discoveries, improvements and inventions
> conceived, discovered, made, designed, researched or developed by the contributor either solely or
> jointly with others, which relate to this work or result from this work. Should any waiver of such
> right be judged legally invalid or ineffective under applicable law, the contributor hereby grants
> to each affected person a royalty-free, non transferable, non sublicensable, non exclusive,
> irrevocable and unconditional license to this right.
